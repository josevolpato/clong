#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# macosx configuration
CND_PLATFORM_macosx=CLang-MacOSX
CND_ARTIFACT_DIR_macosx=dist/macosx/CLang-MacOSX
CND_ARTIFACT_NAME_macosx=clong
CND_ARTIFACT_PATH_macosx=dist/macosx/CLang-MacOSX/clong
CND_PACKAGE_DIR_macosx=dist/macosx/CLang-MacOSX/package
CND_PACKAGE_NAME_macosx=clong.tar
CND_PACKAGE_PATH_macosx=dist/macosx/CLang-MacOSX/package/clong.tar
# linux configuration
CND_PLATFORM_linux=CLang-MacOSX
CND_ARTIFACT_DIR_linux=dist/linux/CLang-MacOSX
CND_ARTIFACT_NAME_linux=clong
CND_ARTIFACT_PATH_linux=dist/linux/CLang-MacOSX/clong
CND_PACKAGE_DIR_linux=dist/linux/CLang-MacOSX/package
CND_PACKAGE_NAME_linux=clong.tar
CND_PACKAGE_PATH_linux=dist/linux/CLang-MacOSX/package/clong.tar
# windows configuration
CND_PLATFORM_windows=CLang-MacOSX
CND_ARTIFACT_DIR_windows=dist/windows/CLang-MacOSX
CND_ARTIFACT_NAME_windows=clong
CND_ARTIFACT_PATH_windows=dist/windows/CLang-MacOSX/clong
CND_PACKAGE_DIR_windows=dist/windows/CLang-MacOSX/package
CND_PACKAGE_NAME_windows=clong.tar
CND_PACKAGE_PATH_windows=dist/windows/CLang-MacOSX/package/clong.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
