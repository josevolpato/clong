/**
 * ScreenSplash.cpp
 * 
 * Copyright (c) 2014 José Volpato
 * 
 * This file is part of Clong.
 *
 * Clong is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clong is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Clong.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "ScreenSplash.hpp"

ScreenSplash::ScreenSplash(Game* game) : Screen(game)
{
    this->itemSelectedIndex = 0;
    
    sf::Texture* texture = game->getGameTexture();    
    this->spTitle.setTexture(*texture);
    this->spTitle.setTextureRect(sf::IntRect(0, 255, 433, 462));
    this->spTitle.setPosition(180, 40);
    this->spNewGame.setTexture(*texture);
    this->spNewGame.setTextureRect(sf::IntRect(128, 80, 169, 29));
    this->spNewGame.setPosition(315, 350);    
    this->spOptions.setTexture(*texture);
    this->spOptions.setTextureRect(sf::IntRect(2, 37, 111, 31));
    this->spOptions.setPosition(340, 400);
    this->spCopyright.setTexture(*texture);
    this->spCopyright.setTextureRect(sf::IntRect(49, 9, 103, 16));
    this->spCopyright.setPosition(345, 550);
}

void ScreenSplash::update()
{
    sf::Event event;
    while (this->game->getWindow()->pollEvent(event))
    {
        if ( event.type == sf::Event::Closed ||
                (event.type == sf::Event::KeyReleased &&
                event.key.code == sf::Keyboard::Escape) )
        {
            this->game->close();
            return;
        }
        
        if ( event.type == sf::Event::KeyReleased &&
            event.key.code == sf::Keyboard::Up)
        {
            if (this->itemSelectedIndex)
            {
                this->itemSelectedIndex = 0;
                this->spNewGame.setTextureRect(sf::IntRect(128, 80, 169, 29));
                this->spOptions.setTextureRect(sf::IntRect(2, 37, 111, 31));
            }
            else
            {
                this->itemSelectedIndex = 1;
                this->spNewGame.setTextureRect(sf::IntRect(128, 39, 169, 29));
                this->spOptions.setTextureRect(sf::IntRect(2, 80, 111, 31));
            }
        }
        
        if ( event.type == sf::Event::KeyReleased &&
            event.key.code == sf::Keyboard::Down)
        {
            if (this->itemSelectedIndex)
            {
                this->itemSelectedIndex = 0;
                this->spNewGame.setTextureRect(sf::IntRect(128, 80, 169, 29));
                this->spOptions.setTextureRect(sf::IntRect(2, 37, 111, 31));
            }
            else
            {
                this->itemSelectedIndex = 1;
                this->spNewGame.setTextureRect(sf::IntRect(128, 39, 169, 29));
                this->spOptions.setTextureRect(sf::IntRect(2, 80, 111, 31));
            }
        }
        
        if ( event.type == sf::Event::KeyReleased &&
            event.key.code == sf::Keyboard::Return)
        {
            if (this->itemSelectedIndex)
            {
                this->game->changeGameState(OPTIONS);
                return;
            }
            else
            {
                this->game->changeGameState(GAME);
                return;
            }
        }
    }    
}

void ScreenSplash::draw()
{
    this->game->getWindow()->clear(sf::Color::Black);
    this->game->getWindow()->draw(this->spTitle);
    this->game->getWindow()->draw(this->spNewGame);
    this->game->getWindow()->draw(this->spOptions);
    this->game->getWindow()->draw(this->spCopyright);    
}