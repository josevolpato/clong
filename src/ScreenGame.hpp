/**
 * ScreenGame.hpp
 * 
 * Copyright (c) 2014 José Volpato
 * 
 * This file is part of Clong.
 *
 * Clong is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clong is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Clong.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef SCREENGAME_H
#define SCREENGAME_H

#include "Screen.hpp"
#include "Game.hpp"
#include "Paddle.hpp"
#include "Ball.hpp"

/**
 * Define os estados possiveis da partida.
 */
enum PlayState
{
    /**
     * Aguardando a contagem regressiva.
     */
    WAITING,
    
    /**
     * Jogo correndo.
     */
    PLAYING,
    
    /**
     * Jogo pausado.
     */
    PAUSED
};

/**
 * Define a tela da partida.
 */
class ScreenGame : public Screen
{
    private:
        Paddle* leftPaddle;
        Paddle* rightPaddle;
        Ball* ball;
        
        sf::Text leftScore;
        sf::Text rightScore;
        
        int leftPaddleScore;
        int rightPaddleScore;
        
        PlayState playState;
        
        sf::RectangleShape pauseRect;
        sf::Text pauseTitle;
        sf::Text pauseResumeGame;
        sf::Text pauseQuitGame;
        int pauseOptionFocus;
        
        sf::Text waitingCount;
        sf::Clock waitingClock;
        sf::Time waitingTime;
        sf::Text previousWinner;
        
        sf::Sound hitSound;
        
    public:
        ScreenGame(Game* game);
        void update();
        void draw();
        ~ScreenGame();
        
        void updateWaiting();
        void updatePlaying();
        void updatePause();
};

#endif // SCREENGAME_H
