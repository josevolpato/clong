/**
 * Screen.hpp
 * 
 * Copyright (c) 2014 José Volpato
 * 
 * This file is part of Clong.
 *
 * Clong is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clong is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Clong.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef SCREEN_H
#define SCREEN_H

class Game;

/**
 * Define uma classe abstrata que serve como base para uma tela do jogo.
 */
class Screen
{
    protected:
        /**
         * Instancia do objeto jogo, usado para o controle do mesmo.
         */
        Game* game;
        
        /**
         * Tamanho da largura da janela, em pixels.
         */
        int resWidth;
        
        /**
         * Tamanho da altura da janela, em pixels. 
         */
        int resHeight;
    
    public:
        /**
         * Cria uma tela para ser usada no jogo.
         * 
         * @param game - Instancia da classe jogo, usado para o controle do
         * mesmo.
         */
        Screen(Game* game);
        
        /**
         * Realiza as rotinas de atualizacao da tela.
         */
        virtual void update() = 0;
        
        /**
         * Realiza as rotinas de desenho da tela.
         */
        virtual void draw() = 0;
};

#endif // SCREEN_H