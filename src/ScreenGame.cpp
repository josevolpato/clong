/**
 * ScreenGame.cpp
 * 
 * Copyright (c) 2014 José Volpato
 * 
 * This file is part of Clong.
 *
 * Clong is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clong is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Clong.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "ScreenGame.hpp"

#include <sstream>
#include <string>


ScreenGame::ScreenGame(Game* game) : Screen(game)
{
    sf::Texture* texture = game->getGameTexture();
    
    sf::Sprite* spLeftPaddle = new sf::Sprite();
    spLeftPaddle->setTexture(*texture);
    if (this->game->getLeftPadModel() == -1)
    {
        this->game->setLeftPadModel(0);
    }
    switch (this->game->getLeftPadModel())
    {
        case 0:
            spLeftPaddle->setTextureRect(sf::IntRect(9, 128, 20, 110));
        break;
        
        case 1:
            spLeftPaddle->setTextureRect(sf::IntRect(41, 128, 20, 110));
        break;
        
        case 2:
            spLeftPaddle->setTextureRect(sf::IntRect(73, 128, 20, 110));
        break;
        
        case 3:
            spLeftPaddle->setTextureRect(sf::IntRect(107, 128, 20, 110));
        break;
        
        case 4:
            spLeftPaddle->setTextureRect(sf::IntRect(143, 128, 20, 110));
        break;
        
        case 5:
            spLeftPaddle->setTextureRect(sf::IntRect(179, 128, 20, 110));
        break;
        
        case 6:
            spLeftPaddle->setTextureRect(sf::IntRect(215, 128, 20, 110));
        break;
        
        case 7:
            spLeftPaddle->setTextureRect(sf::IntRect(251, 128, 20, 110));
        break;
        
        case 8:
            spLeftPaddle->setTextureRect(sf::IntRect(286, 128, 20, 110));
        break;
    }
    this->leftPaddle = new Paddle(5, this->resHeight/2, spLeftPaddle);
    
    sf::Sprite* spRightPaddle = new sf::Sprite();
    spRightPaddle->setTexture(*texture);
    if (this->game->getRightPadModel() == -1)
    {
        this->game->setRightPadModel(1);
    }
    switch (this->game->getRightPadModel())
    {
        case 0:
            spRightPaddle->setTextureRect(sf::IntRect(9, 128, 20, 110));
        break;
        
        case 1:
            spRightPaddle->setTextureRect(sf::IntRect(41, 128, 20, 110));
        break;
        
        case 2:
            spRightPaddle->setTextureRect(sf::IntRect(73, 128, 20, 110));
        break;
        
        case 3:
            spRightPaddle->setTextureRect(sf::IntRect(107, 128, 20, 110));
        break;
        
        case 4:
            spRightPaddle->setTextureRect(sf::IntRect(143, 128, 20, 110));
        break;
        
        case 5:
            spRightPaddle->setTextureRect(sf::IntRect(179, 128, 20, 110));
        break;
        
        case 6:
            spRightPaddle->setTextureRect(sf::IntRect(215, 128, 20, 110));
        break;
        
        case 7:
            spRightPaddle->setTextureRect(sf::IntRect(251, 128, 20, 110));
        break;
        
        case 8:
            spRightPaddle->setTextureRect(sf::IntRect(286, 128, 20, 110));
        break;
    }
    this->rightPaddle = new Paddle(this->resWidth - 25, this->resHeight/2, spRightPaddle);
    
    this->ball = new Ball(this->resWidth/2, this->resHeight/2, 10, sf::Color::Yellow);
    
    sf::SoundBuffer* hitSoundBuffer = this->game->getHitSoundBuffer();
    this->hitSound.setBuffer(*hitSoundBuffer);
    
    this->leftPaddleScore = 0;
    this->leftScore.setFont(*this->game->getOptionFont());
    this->leftScore.setCharacterSize(75);
    this->leftScore.setColor(sf::Color::Yellow);
    std::stringstream lss;
    lss << this->leftPaddleScore;
    std::string lScore = lss.str();
    this->leftScore.setString(lScore);
    this->leftScore.setPosition(340, 10);
    
    this->rightPaddleScore = 0;
    this->rightScore.setFont(*this->game->getOptionFont());
    this->rightScore.setCharacterSize(75);
    this->rightScore.setColor(sf::Color::Yellow);
    std::stringstream rss;
    rss << this->rightPaddleScore;
    std::string rScore = rss.str();
    this->rightScore.setString(rScore);
    this->rightScore.setPosition(410, 10);
    
    this->pauseRect.setPosition(0, 0);
    this->pauseRect.setSize(sf::Vector2f(800, 600));
    this->pauseRect.setFillColor(sf::Color(0, 0, 0, 128));
    this->pauseTitle.setFont(*this->game->getOptionFont());
    this->pauseTitle.setCharacterSize(75);
    this->pauseTitle.setColor(sf::Color::Yellow);
    this->pauseTitle.setString("PAUSE");
    this->pauseTitle.setPosition(300, 10);
    this->pauseResumeGame.setFont(*this->game->getOptionFont());
    this->pauseResumeGame.setCharacterSize(50);
    this->pauseResumeGame.setColor(sf::Color::Yellow);
    this->pauseResumeGame.setString("Continuar jogo");
    this->pauseResumeGame.setPosition(240, 200);
    this->pauseQuitGame.setFont(*this->game->getOptionFont());
    this->pauseQuitGame.setCharacterSize(50);
    this->pauseQuitGame.setColor(sf::Color::White);
    this->pauseQuitGame.setString("Sair do jogo");
    this->pauseQuitGame.setPosition(265, 300);
    this->pauseOptionFocus = 0;
    
    this->waitingCount.setFont(*this->game->getOptionFont());
    this->waitingCount.setCharacterSize(250);
    this->waitingCount.setColor(sf::Color::Yellow);
    this->waitingCount.setString("3");
    this->waitingCount.setPosition(340, 100);
    
    this->waitingClock.restart();
    this->waitingTime = this->waitingClock.getElapsedTime();
   
    this->previousWinner.setFont(*this->game->getOptionFont());
    this->previousWinner.setCharacterSize(50);
    this->previousWinner.setColor(sf::Color::Yellow);
    this->previousWinner.setString("");    
    
    this->playState = WAITING;
}

void ScreenGame::update()
{
    // Verifica os eventos de janela
    sf::Event event;
    while (this->game->getWindow()->pollEvent(event))
    {
        if (event.type == sf::Event::Closed)
        {
            this->game->close();
            return;
        }
        
        if (event.type == sf::Event::KeyReleased)
        {
            switch (event.key.code)
            {
                case sf::Keyboard::Escape:
                    switch (this->playState)
                    {
                        case WAITING:
                        break;

                        case PLAYING:
                            this->playState = PAUSED;
                        break;

                        case PAUSED:
                            this->game->changeGameState(SPLASH_SCREEN);
                        return;
                    }
                break;
                
                case sf::Keyboard::Return:
                    switch (this->playState)
                    {
                        case WAITING:
                        break;

                        case PLAYING:
                        break;

                        case PAUSED:
                            if (this->pauseOptionFocus == 0)
                            {
                                this->playState = PLAYING;
                            }
                            else
                            {
                                this->game->changeGameState(SPLASH_SCREEN);
                                return;
                            }
                        break;
                    }
                break;
                
                case sf::Keyboard::Up:
                    switch (this->playState)
                    {
                        case WAITING:
                        break;

                        case PLAYING:
                        break;

                        case PAUSED:
                            if (this->pauseOptionFocus == 0)
                            {
                                this->pauseOptionFocus = 1;
                            }
                            else
                            {
                                this->pauseOptionFocus = 0;
                            }
                        break;
                    }
                break;
                
                case sf::Keyboard::Down:
                    switch (this->playState)
                    {
                        case WAITING:
                        break;

                        case PLAYING:
                        break;

                        case PAUSED:
                            if (this->pauseOptionFocus == 0)
                            {
                                this->pauseOptionFocus = 1;
                            }
                            else
                            {
                                this->pauseOptionFocus = 0;
                            }
                        break;
                    }
                break;
                
            }
        }
    }
    
    switch (this->playState)
    {
        // 3, 2, 1
        case WAITING:
            updateWaiting();
        break;
        
        // Jogando
        case PLAYING:
            updatePlaying();
        break;
        
        // Pause
        case PAUSED:
            updatePause();
        break;
    }
    
}

void ScreenGame::updateWaiting()
{
    this->waitingTime = this->waitingClock.getElapsedTime();
    if (this->waitingTime.asSeconds() >= 1.0)
    {
        this->waitingCount.setString("2");
        this->waitingCount.setColor(sf::Color(255, 128, 0, 255));
    }
    
    if (this->waitingTime.asSeconds() >= 2.0)
    {
        this->waitingCount.setString("1");
        this->waitingCount.setColor(sf::Color::Red);
    }
    
    if (this->waitingTime.asSeconds() >= 3.0)
    {
        this->playState = PLAYING;
    }
    
    // Movimento raquete esquerda
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
    {
        leftPaddle->setY(this->leftPaddle->getY() - 10);
        if (leftPaddle->getY() < 0)
        {
            leftPaddle->setY(0);
        }
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Z))
    {
        leftPaddle->setY(this->leftPaddle->getY() + 10);
        if ((leftPaddle->getY() + leftPaddle->PADDLE_HEIGHT) > resHeight)
        {
            leftPaddle->setY(resHeight - leftPaddle->PADDLE_HEIGHT);
        }
    }
    
    // Movimento raquete direita
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
    {
        rightPaddle->setY(this->rightPaddle->getY() - 10);
        if (rightPaddle->getY() < 0)
        {
            rightPaddle->setY(0);
        }
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
    {
        this->rightPaddle->setY(this->rightPaddle->getY() + 10);
        if ((rightPaddle->getY() + rightPaddle->PADDLE_HEIGHT) > resHeight)
        {
            rightPaddle->setY(resHeight - rightPaddle->PADDLE_HEIGHT);
        }
    }
}

void ScreenGame::updatePlaying()
{
    // Movimento raquete esquerda
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
    {
        leftPaddle->setY(this->leftPaddle->getY() - 10);
        if (leftPaddle->getY() < 0)
        {
            leftPaddle->setY(0);
        }
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Z))
    {
        leftPaddle->setY(this->leftPaddle->getY() + 10);
        if ((leftPaddle->getY() + leftPaddle->PADDLE_HEIGHT) > resHeight)
        {
            leftPaddle->setY(resHeight - leftPaddle->PADDLE_HEIGHT);
        }
    }
    
    // Movimento raquete direita
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
    {
        rightPaddle->setY(this->rightPaddle->getY() - 10);
        if (rightPaddle->getY() < 0)
        {
            rightPaddle->setY(0);
        }
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
    {
        this->rightPaddle->setY(this->rightPaddle->getY() + 10);
        if ((rightPaddle->getY() + rightPaddle->PADDLE_HEIGHT) > resHeight)
        {
            rightPaddle->setY(resHeight - rightPaddle->PADDLE_HEIGHT);
        }
    }

    // Movimento da bola
    // Vertical
    if (ball->getVerticalDirection())
    {
        ball->setY(ball->getY() - this->game->getBallSpeed());
        if (ball->getY() < 0)
        {
            ball->invertVerticalDirection();
            hitSound.play();
        }
    }
    else
    {
        ball->setY(ball->getY() + this->game->getBallSpeed());
        if (ball->getY() > resHeight)
        {
            ball->invertVerticalDirection();
            hitSound.play();
        }
    }

    // Horizontal
    if (ball->getHorizontalDirection())
    {
        ball->setX(ball->getX() - this->game->getBallSpeed());

        if (ball->getX() <= (leftPaddle->getX() + leftPaddle->PADDLE_WIDTH) &&
                (ball->getX() + ball->getRadius() * 2) >= leftPaddle->getX() &&
                (ball->getY() + ball->getRadius() * 2) >= leftPaddle->getY() &&
                ball->getY() <= leftPaddle->getY() + leftPaddle->PADDLE_HEIGHT)
        {
            this->ball->invertHorizontalDirection();
            hitSound.play();
        }

        // Ponto para a raquete da direita
        if ((ball->getX() + (2 * ball->getRadius())) < 0)
        {
            this->rightPaddleScore++;
            std::stringstream rss;
            rss << this->rightPaddleScore;
            std::string rScore = rss.str();
            this->rightScore.setString(rScore);
            this->ball->setPosition(this->resWidth/2, this->resHeight/2);
            this->ball->invertHorizontalDirection();
        }
    }
    else
    {
        ball->setX(ball->getX() + this->game->getBallSpeed());

        // Colisao com a raquete da direita
        if ( (ball->getX() + (ball->getRadius() * 2)) >= rightPaddle->getX() &&
                ball->getX() <= (rightPaddle->getX() + rightPaddle->PADDLE_WIDTH) &&
                (ball->getY() + (ball->getRadius() * 2)) >= rightPaddle->getY() &&
                ball->getY() <= (rightPaddle->getY() + rightPaddle->PADDLE_HEIGHT) )
        {
            this->ball->invertHorizontalDirection();
            hitSound.play();
        }

        // Ponto para a raquete da esquerda
        if (ball->getX() > resWidth)
        {
            this->leftPaddleScore++;
            std::stringstream lss;
            lss << this->leftPaddleScore;
            std::string lScore = lss.str();
            this->leftScore.setString(lScore);
            this->ball->setPosition(this->resWidth/2, this->resHeight/2);
            this->ball->invertHorizontalDirection();
        }
    }

    //Fim de jogo
    if (this->leftPaddleScore == 7)
    {
        // Reinicia o jogo
        this->leftPaddleScore = 0;
        std::stringstream lss;
        lss << this->leftPaddleScore;
        std::string lScore = lss.str();
        this->leftScore.setString(lScore);
        
        this->rightPaddleScore = 0;
        std::stringstream rss;
        rss << this->rightPaddleScore;
        std::string rScore = rss.str();
        this->rightScore.setString(rScore);
        
        this->previousWinner.setPosition(80, 85);
        this->previousWinner.setString("O jogador da esquerda venceu!");
        
        this->waitingCount.setString("3");
        this->waitingCount.setColor(sf::Color::Yellow);
        this->waitingClock.restart();
        this->waitingTime = this->waitingClock.getElapsedTime();
        this->playState = WAITING;
    }
    else
    {
         if (this->rightPaddleScore == 7)
         {
             // Reinicia o jogo
            this->leftPaddleScore = 0;
            std::stringstream lss;
            lss << this->leftPaddleScore;
            std::string lScore = lss.str();
            this->leftScore.setString(lScore);

            this->rightPaddleScore = 0;
            std::stringstream rss;
            rss << this->rightPaddleScore;
            std::string rScore = rss.str();
            this->rightScore.setString(rScore);
            
            this->previousWinner.setPosition(100, 85);
            this->previousWinner.setString("O jogador da direita venceu!");
            
            this->waitingCount.setString("3");
            this->waitingCount.setColor(sf::Color::Yellow);
            this->waitingClock.restart();
            this->waitingTime = this->waitingClock.getElapsedTime();
            this->playState = WAITING;
         }
    }
}

void ScreenGame::updatePause()
{
    switch (this->pauseOptionFocus)
    {
        case 0:
            this->pauseResumeGame.setColor(sf::Color::Yellow);
            this->pauseQuitGame.setColor(sf::Color::White);
        break;
        
        case 1:
            this->pauseResumeGame.setColor(sf::Color::White);
            this->pauseQuitGame.setColor(sf::Color::Yellow);
        break;
    }
}

void ScreenGame::draw()
{   
    this->game->getWindow()->clear(sf::Color::Black);
    
    this->game->getWindow()->draw(*leftPaddle->getSprite());
    this->game->getWindow()->draw(*rightPaddle->getSprite());
    
    switch (this->playState)
    {
        case WAITING:
            this->game->getWindow()->draw(this->previousWinner);
            this->game->getWindow()->draw(this->waitingCount);
        break;
        
        case PLAYING:
            this->game->getWindow()->draw(*ball);
            this->game->getWindow()->draw(this->leftScore);
            this->game->getWindow()->draw(this->rightScore);
        break;
        
        case PAUSED:
            this->game->getWindow()->draw(*ball);
            this->game->getWindow()->draw(this->leftScore);
            this->game->getWindow()->draw(this->rightScore);
            this->game->getWindow()->draw(this->pauseRect);
            this->game->getWindow()->draw(this->pauseTitle);
            this->game->getWindow()->draw(this->pauseResumeGame);
            this->game->getWindow()->draw(this->pauseQuitGame);
        break;
    }
}

ScreenGame::~ScreenGame()
{
    delete this->leftPaddle;
    delete this->rightPaddle;
    delete this->ball;
}
