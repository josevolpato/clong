/**
 * ScreenSplash.hpp
 * 
 * Copyright (c) 2014 José Volpato
 * 
 * This file is part of Clong.
 *
 * Clong is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clong is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Clong.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef SCREENSPLASH_H
#define SCREENSPLASH_H

#include "Screen.hpp"
#include "Game.hpp"

/**
 * Classe da tela de abertura do jogo.
 */
class ScreenSplash : public Screen
{
    private:
        sf::Sprite spTitle;
        sf::Sprite spNewGame;
        sf::Sprite spOptions;
        sf::Sprite spCopyright;
        
        int itemSelectedIndex;
        
    public:
        ScreenSplash(Game* game);
        void update();
        void draw();
};

#endif // SCREENSPLASH_H