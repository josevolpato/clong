/**
 * Gamestate.hpp
 * 
 * Copyright (c) 2014 José Volpato
 * 
 * This file is part of Clong.
 *
 * Clong is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clong is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Clong.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef GAMESTATE_H
#define	GAMESTATE_H

/**
 * Define os estados do jogo.
 */
enum GAME_STATE
{
    // Tela de abertura
    SPLASH_SCREEN,
    
    // Tela de opcoes
    OPTIONS,
    
    // Tela de jogo
    GAME
};

#endif	// GAMESTATE_H