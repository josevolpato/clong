/**
 * ScreenOptions.cpp
 * 
 * Copyright (c) 2014 José Volpato
 * 
 * This file is part of Clong.
 *
 * Clong is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clong is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Clong.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "ScreenOptions.hpp"
#include "Game.hpp"

ScreenOptions::ScreenOptions(Game* game) : Screen(game)
{
    this->focusIndex = UPLEFT;
    if (this->game->getLeftPadModel() == -1)
    {
        this->leftPaddleSkin = 0;
    }
    else
    {
        this->leftPaddleSkin = this->game->getLeftPadModel();
    }
    if (this->game->getRightPadModel() == -1)
    {
        this->rightPaddleSkin = 1;
    }
    else
    {
        this->rightPaddleSkin = this->game->getRightPadModel();
    }
    this->ballSpeed = this->game->getBallSpeed();
    
    this->lblOptions = new sf::Text(L"OPÇÕES", *this->game->getOptionFont());
    this->lblOptions->setCharacterSize(50);
    this->lblOptions->setColor(sf::Color::Yellow);
    this->lblOptions->setPosition(300, 10);
    
    this->lblPaddleSkin = new sf::Text("Estilo de raquete", *this->game->getOptionFont());
    this->lblPaddleSkin->setCharacterSize(25);
    this->lblPaddleSkin->setColor(sf::Color::Yellow);
    this->lblPaddleSkin->setPosition(90, 100);
    
    this->lblBallSpeed = new sf::Text("Velocidade da bola", *this->game->getOptionFont());
    this->lblBallSpeed->setCharacterSize(25);
    this->lblBallSpeed->setColor(sf::Color::Yellow);
    this->lblBallSpeed->setPosition(440, 100);
    
    sf::Texture* texture = this->game->getGameTexture();
    this->leftPadModelUp.setTexture(*texture);
    this->leftPadModelUp.setTextureRect(sf::IntRect(161, 4, 60, 25));
    this->leftPadModelUp.setPosition(100, 150);
    
    this->leftPadModelDown.setTexture(*texture);
    this->leftPadModelDown.setTextureRect(sf::IntRect(225, 4, 60, 25));
    this->leftPadModelDown.setPosition(100, 350);
    
    this->leftPadModel.setTexture(*texture);
    this->leftPadModel.setTextureRect(sf::IntRect(9, 128, 20, 110));
    this->leftPadModel.setPosition(120, 210);
    
    this->rightPadModelUp.setTexture(*texture);
    this->rightPadModelUp.setTextureRect(sf::IntRect(161, 4, 60, 25));
    this->rightPadModelUp.setPosition(200, 150);
    
    this->rightPadModelDown.setTexture(*texture);
    this->rightPadModelDown.setTextureRect(sf::IntRect(225, 4, 60, 25));
    this->rightPadModelDown.setPosition(200, 350);
    
    this->rightPadModel.setTexture(*texture);
    this->rightPadModel.setTextureRect(sf::IntRect(41, 128, 20, 110));
    this->rightPadModel.setPosition(220, 210);
    
    this->minusSpeed.setTexture(*texture);
    this->minusSpeed.setTextureRect(sf::IntRect(329, 44, 20, 20));
    this->minusSpeed.setPosition(350, 120);
    
    this->plusSpeed.setTexture(*texture);
    this->plusSpeed.setTextureRect(sf::IntRect(305, 44, 20, 20));
    this->plusSpeed.setPosition(730, 120);
    
    this->playArea.setPosition(350, 150);
    this->playArea.setSize(sf::Vector2f(400, 400));
    this->playArea.setOutlineThickness(3);
    this->playArea.setOutlineColor(sf::Color::Yellow);
    this->playArea.setFillColor(sf::Color::Transparent);
    
    this->ball = new Ball(500, 300, 10, sf::Color::Yellow);
}

void ScreenOptions::update()
{
    sf::Event event;
    while (this->game->getWindow()->pollEvent(event))
    {
        if (event.type == sf::Event::Closed)
        {
            this->game->close();
            return;
        }
        
        if (event.type == sf::Event::KeyReleased)
        {
            if (event.key.code == sf::Keyboard::Escape)
            {
                this->game->setBallSpeed(this->ballSpeed);
                this->game->setLeftPadModel(this->leftPaddleSkin);
                this->game->setRightPadModel(this->rightPaddleSkin);
                this->game->changeGameState(SPLASH_SCREEN);
                return;
            }
            
            if (event.key.code == sf::Keyboard::Up)
            {
                switch (focusIndex)
                {
                    case UPLEFT:
                        this->focusIndex = DOWNLEFT;
                    break;

                    case DOWNLEFT:
                        this->focusIndex = UPLEFT;
                    break;

                    case UPRIGHT:
                        this->focusIndex = DOWNRIGHT;
                    break;

                    case DOWNRIGHT:
                        this->focusIndex = UPRIGHT;
                    break;

                    case MINUSSPEED:
                    case PLUSSPEED:
                    break;
                }
                return;
            }
            
            if (event.key.code == sf::Keyboard::Down)
            {
                switch (focusIndex)
                {
                    case UPLEFT:
                        this->focusIndex = DOWNLEFT;
                    break;

                    case DOWNLEFT:
                        this->focusIndex = UPLEFT;
                    break;

                    case UPRIGHT:
                        this->focusIndex = DOWNRIGHT;
                    break;

                    case DOWNRIGHT:
                        this->focusIndex = UPRIGHT;
                    break;

                    case MINUSSPEED:
                    case PLUSSPEED:
                    break;
                }
                return;
            }
            
            if (event.key.code == sf::Keyboard::Left)
            {
                switch (focusIndex)
                {
                    case UPLEFT:
                        this->focusIndex = PLUSSPEED;
                    break;

                    case DOWNLEFT:
                        this->focusIndex = PLUSSPEED;
                    break;

                    case UPRIGHT:
                        this->focusIndex = UPLEFT;
                    break;

                    case DOWNRIGHT:
                        this->focusIndex = DOWNLEFT;
                    break;

                    case MINUSSPEED:
                        this->focusIndex = UPRIGHT;
                    break;

                    case PLUSSPEED:
                        this->focusIndex = MINUSSPEED;
                    break;
                }
                return;
            }
            
            if (event.key.code == sf::Keyboard::Right)
            {
                switch (focusIndex)
                {
                    case UPLEFT:
                        this->focusIndex = UPRIGHT;
                    break;

                    case DOWNLEFT:
                        this->focusIndex = DOWNRIGHT;
                    break;

                    case UPRIGHT:
                        this->focusIndex = MINUSSPEED;
                    break;

                    case DOWNRIGHT:
                        this->focusIndex = MINUSSPEED;
                    break;

                    case MINUSSPEED:
                        this->focusIndex = PLUSSPEED;
                    break;

                    case PLUSSPEED:
                        this->focusIndex = UPLEFT;
                    break;
                }
                return;
            }

            if (event.key.code == sf::Keyboard::Return)
            {
                switch (this->focusIndex)
                {
                    case UPLEFT:
                        this->leftPaddleSkin--;
                        if (this->leftPaddleSkin == -1)
                        {
                            this->leftPaddleSkin = 8;
                        }
                    break;

                    case DOWNLEFT:
                        this->leftPaddleSkin++;
                        if (this->leftPaddleSkin == 9)
                        {
                            this->leftPaddleSkin = 0;
                        }
                    break;

                    case UPRIGHT:
                        this->rightPaddleSkin--;
                        if (this->rightPaddleSkin == -1)
                        {
                            this->rightPaddleSkin = 8;
                        }
                    break;

                    case DOWNRIGHT:
                        this->rightPaddleSkin++;
                        if (this->rightPaddleSkin == 9)
                        {
                            this->rightPaddleSkin = 0;
                        }
                    break;

                    case MINUSSPEED:
                        this->ballSpeed = this->ballSpeed - 2;
                        if (this->ballSpeed < 3)
                        {
                            this->ballSpeed = 3;
                        }
                    break;

                    case PLUSSPEED:
                        this->ballSpeed = this->ballSpeed + 2;
                        if (this->ballSpeed > 15)
                        {
                            this->ballSpeed = 15;
                        }
                    break;        
                }
                return;
            }
        }       
    }
    
    // Movimento da bola
    // Vertical
    if (this->ball->getVerticalDirection())
    {
        this->ball->setY(this->ball->getY() - ballSpeed);
        if (this->ball->getY() < this->playArea.getPosition().y)
        {
            this->ball->invertVerticalDirection();
        }
    }
    else
    {
        this->ball->setY(this->ball->getY() + ballSpeed);
        if ((this->ball->getY() + this->ball->getRadius() * 2) > (this->playArea.getPosition().y + this->playArea.getSize().y))
        {
            this->ball->invertVerticalDirection();
        }
    }
    
    // Horizontal
    if (this->ball->getHorizontalDirection())
    {
        this->ball->setX(this->ball->getX() - ballSpeed);
        if (this->ball->getX() < this->playArea.getPosition().x)
        {
            this->ball->invertHorizontalDirection();
        }
    }
    else
    {
        this->ball->setX(this->ball->getX() + ballSpeed);
        if ((this->ball->getX() + this->ball->getRadius() * 2) > (this->playArea.getPosition().x + this->playArea.getSize().x))
        {
            this->ball->invertHorizontalDirection();
        }
    }
    
    this->leftPadModelUp.setTextureRect(sf::IntRect(161, 4, 60, 25));
    this->leftPadModelDown.setTextureRect(sf::IntRect(225, 4, 60, 25));
    this->rightPadModelUp.setTextureRect(sf::IntRect(161, 4, 60, 25));
    this->rightPadModelDown.setTextureRect(sf::IntRect(225, 4, 60, 25));
    this->minusSpeed.setTextureRect(sf::IntRect(329, 44, 20, 20));
    this->plusSpeed.setTextureRect(sf::IntRect(305, 44, 20, 20));
    
    switch (this->focusIndex)
    {
        case UPLEFT:
            this->leftPadModelUp.setTextureRect(sf::IntRect(289, 4, 60, 25));
        break;
        
        case DOWNLEFT:
            this->leftPadModelDown.setTextureRect(sf::IntRect(353, 4, 60, 25));
        break;
        
        case UPRIGHT:
            this->rightPadModelUp.setTextureRect(sf::IntRect(289, 4, 60, 25));
        break;
        
        case DOWNRIGHT:
            this->rightPadModelDown.setTextureRect(sf::IntRect(353, 4, 60, 25));
        break;
        
        case MINUSSPEED:
            this->minusSpeed.setTextureRect(sf::IntRect(377, 44, 20, 20));
        break;
        
        case PLUSSPEED:
            this->plusSpeed.setTextureRect(sf::IntRect(353, 44, 20, 20));
        break;        
    }
    
    switch (this->leftPaddleSkin)
    {
        case 0:
            this->leftPadModel.setTextureRect(sf::IntRect(9, 128, 20, 110));
        break;
        
        case 1:
            this->leftPadModel.setTextureRect(sf::IntRect(41, 128, 20, 110));
        break;
        
        case 2:
            this->leftPadModel.setTextureRect(sf::IntRect(73, 128, 20, 110));
        break;
        
        case 3:
            this->leftPadModel.setTextureRect(sf::IntRect(107, 128, 20, 110));
        break;
        
        case 4:
            this->leftPadModel.setTextureRect(sf::IntRect(143, 128, 20, 110));
        break;
        
        case 5:
            this->leftPadModel.setTextureRect(sf::IntRect(179, 128, 20, 110));
        break;
        
        case 6:
            this->leftPadModel.setTextureRect(sf::IntRect(215, 128, 20, 110));
        break;
        
        case 7:
            this->leftPadModel.setTextureRect(sf::IntRect(251, 128, 20, 110));
        break;
        
        case 8:
            this->leftPadModel.setTextureRect(sf::IntRect(286, 128, 20, 110));
        break;
    }
    
    switch (this->rightPaddleSkin)
    {
        case 0:
            this->rightPadModel.setTextureRect(sf::IntRect(9, 128, 20, 110));
        break;
        
        case 1:
            this->rightPadModel.setTextureRect(sf::IntRect(41, 128, 20, 110));
        break;
        
        case 2:
            this->rightPadModel.setTextureRect(sf::IntRect(73, 128, 20, 110));
        break;
        
        case 3:
            this->rightPadModel.setTextureRect(sf::IntRect(107, 128, 20, 110));
        break;
        
        case 4:
            this->rightPadModel.setTextureRect(sf::IntRect(143, 128, 20, 110));
        break;
        
        case 5:
            this->rightPadModel.setTextureRect(sf::IntRect(179, 128, 20, 110));
        break;
        
        case 6:
            this->rightPadModel.setTextureRect(sf::IntRect(215, 128, 20, 110));
        break;
        
        case 7:
            this->rightPadModel.setTextureRect(sf::IntRect(251, 128, 20, 110));
        break;
        
        case 8:
            this->rightPadModel.setTextureRect(sf::IntRect(286, 128, 20, 110));
        break;
    }
    
}

void ScreenOptions::draw()
{    
    this->game->getWindow()->clear(sf::Color::Black);
    this->game->getWindow()->draw(*this->lblOptions);
    this->game->getWindow()->draw(*this->lblPaddleSkin);
    this->game->getWindow()->draw(*this->lblBallSpeed);
    this->game->getWindow()->draw(this->leftPadModelUp);
    this->game->getWindow()->draw(this->leftPadModelDown);
    this->game->getWindow()->draw(this->leftPadModel);
    this->game->getWindow()->draw(this->rightPadModelUp);
    this->game->getWindow()->draw(this->rightPadModelDown);
    this->game->getWindow()->draw(this->rightPadModel);
    this->game->getWindow()->draw(this->plusSpeed);
    this->game->getWindow()->draw(this->minusSpeed);
    this->game->getWindow()->draw(this->playArea);
    this->game->getWindow()->draw(*this->ball);
}

ScreenOptions::~ScreenOptions()
{
    delete this->lblOptions;
    delete this->lblPaddleSkin;
    delete this->lblBallSpeed;
    delete this->ball;
}