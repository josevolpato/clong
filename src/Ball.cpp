/**
 * Ball.cpp
 * 
 * Copyright (c) 2014 José Volpato
 * 
 * This file is part of Clong.
 *
 * Clong is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clong is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Clong.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Ball.hpp"

Ball::Ball(int x, int y, int radius, sf::Color fillColor)
{
    this->setRadius(radius);
    this->setPosition(x, y);
    this->setFillColor(fillColor);
    
    this->hDirection = 0;
    this->vDirection = 0;
}

int Ball::getX()
{
    return this->getPosition().x;
}

void Ball::setX(int x)
{
    this->setPosition(x, this->getPosition().y);
}

int Ball::getY()
{
    return this->getPosition().y;
}

void Ball::setY(int y)
{
    this->setPosition(this->getPosition().x, y);
}

sf::Color Ball::getColor()
{
    this->getFillColor();
}

void Ball::invertHorizontalDirection()
{
    if (this->hDirection)
    {
        this->hDirection = 0;
    }
    else
    {
        this->hDirection = 1;
    }
}

void Ball::invertVerticalDirection()
{
    if (this->vDirection)
    {
        this->vDirection = 0;
    }
    else
    {
        this->vDirection = 1;
    }
}

int Ball::getHorizontalDirection()
{
    return this->hDirection;
}

int Ball::getVerticalDirection()
{
    return this->vDirection;
}