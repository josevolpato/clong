/**
 * ScreenOptions.hpp
 * 
 * Copyright (c) 2014 José Volpato
 * 
 * This file is part of Clong.
 *
 * Clong is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clong is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Clong.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef SCREENOPTIONS_H
#define SCREENOPTIONS_H

#include <SFML/Graphics.hpp>
#include "Screen.hpp"
#include "Ball.hpp"

/**
 * Define a posicao dos botoes na lista de indices de focus.
 */
enum IndexPosition
{
    UPLEFT,
    DOWNLEFT,
    UPRIGHT,
    DOWNRIGHT,
    MINUSSPEED,
    PLUSSPEED    
};

/**
 * Define a tela de opcoes do jogo.
 */
class ScreenOptions : public Screen
{
    private:
        sf::Text* lblOptions;
        sf::Text* lblPaddleSkin;
        sf::Text* lblBallSpeed;
        
        sf::Sprite leftPadModelUp;
        sf::Sprite leftPadModelDown;
        sf::Sprite leftPadModel;
        sf::Sprite rightPadModelUp;
        sf::Sprite rightPadModelDown;
        sf::Sprite rightPadModel;
        sf::Sprite plusSpeed;
        sf::Sprite minusSpeed;
        
        sf::RectangleShape playArea;
        Ball* ball;
        
        IndexPosition focusIndex;
        
        int leftPaddleSkin;
        int rightPaddleSkin;
        
        float ballSpeed;
        
    public:
        ScreenOptions(Game* game);
        void update();
        void draw();
        ~ScreenOptions();
};

#endif // SCREENOPTIONS_H
