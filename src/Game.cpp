/**
 * Game.cpp
 * 
 * Copyright (c) 2014 José Volpato
 * 
 * This file is part of Clong.
 *
 * Clong is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clong is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Clong.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Game.hpp"
#include "ScreenGame.hpp"
#include "ScreenSplash.hpp"
#include "ScreenOptions.hpp"

Game::Game(int resWidth, int resHeight)
{
    this->resWidth = resWidth;
    this->resHeight = resHeight;
    this->gameState = SPLASH_SCREEN;
    
    this->ballSpeed = 5;
    this->leftPadModel = -1;
    this->rightPadModel = -1;
    
    // Inicializa a janela
    this->window = new sf::RenderWindow(
            sf::VideoMode(this->resWidth, this->resHeight, 32),
            "Clong",
            sf::Style::Titlebar | sf::Style::Close);
    this->window->setFramerateLimit(60);
    
    this->gameTexture.loadFromFile("resources/drawable/clong_texture.png");
    
    this->gameHitSoundBuffer;
    this->gameHitSoundBuffer.loadFromFile("resources/audio/hit.wav");
    
    // Inicializa a tela (tela de inicio)
    this->screen = new ScreenSplash(this);
}

void Game::start()
{
    // Loop do jogo
    while (this->window->isOpen())
    {
        this->clock.restart();
        update();        
        if (!(this->window->isOpen()))
        {
            return;
        }
        
        draw();
        control();
    }
}

void Game::update()
{    
    this->screen->update();
}

void Game::draw()
{
    this->screen->draw();
    this->window->display();
}

void Game::control()
{
    
}

void Game::changeGameState(GAME_STATE gameState)
{
    this->gameState = gameState;
    
    switch (this->gameState)
    {
        case SPLASH_SCREEN:
            this->screen = new ScreenSplash(this);
        break;
        
        case OPTIONS:
            this->screen = new ScreenOptions(this);
        break;
            
        case GAME:
            this->screen = new ScreenGame(this);
        break;
    }
}

sf::Texture* Game::getGameTexture()
{
    return &this->gameTexture;
}

sf::SoundBuffer* Game::getHitSoundBuffer()
{
    return &this->gameHitSoundBuffer;
}

sf::RenderWindow* Game::getWindow()
{
    return this->window;
}

void Game::close()
{
    this->window->close();
}

void Game::setLeftPadModel(int padModel)
{
    this->leftPadModel = padModel;
}

int Game::getLeftPadModel()
{
    return this->leftPadModel;
}

void Game::setRightPadModel(int padModel)
{
    this->rightPadModel = padModel;
}

int Game::getRightPadModel()
{
    return this->rightPadModel;
}

sf::Font* Game::getOptionFont()
{
    sf::Font* font = new sf::Font();
    font->loadFromFile("resources/font/orange_juice.ttf");
    return font;
}

void Game::setBallSpeed(int ballSpeed)
{
    this->ballSpeed = ballSpeed;
}

int Game::getBallSpeed()
{
    return this->ballSpeed;
}

Game::~Game()
{
    delete this->screen;
    delete this->window;
}