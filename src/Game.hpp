/**
 * Games.hpp
 * 
 * Copyright (c) 2014 José Volpato
 * 
 * This file is part of Clong.
 *
 * Clong is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clong is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Clong.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef GAME_H
#define	GAME_H

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

#include "Screen.hpp"
#include "Gamestate.hpp"

/**
 * Classe de controle do jogo.
 */
class Game
{
    private:
        // Janela do jogo
        int resWidth;
        int resHeight;
        sf::RenderWindow* window;
        
        // Variaveis de controle de estado
        GAME_STATE gameState;
        Screen* screen;
        
        // Textura contendo os graficos do jogo
        sf::Texture gameTexture;
        
        // Timer do jogo
        sf::Clock clock;
        
        // Buffer contendo o som de colisao do jogo
        sf::SoundBuffer gameHitSoundBuffer;
        
        // Indica quais graficos de raquete cada raquete usara
        int leftPadModel;
        int rightPadModel;
        
        // Indica qual sera a velocidade da bola
        float ballSpeed;
        
        /**
         * Recebe os eventos do jogo e atualiza os objetos do jogo.
         */
        void update();
        
        /**
         * Desenha os objetos do jogo na tela.
         */
        void draw();
        
        /**
         * Realiza o controle de frame.
         */
        void control();
    
    public:
        /**
         * Inicializa a classe de controle e os componentes do jogo.
         * 
         * @param resWidth - Largura da tela em pixels.
         * @param resHeight - Altura da tela em pixels.
         */
        Game(int resWidth, int resHeight);
        
        /**
         * Finaliza os componentes do jogo.
         */
        ~Game();
        
        /**
         * Processa o loop do jogo e finaliza o jogo quando requisitado.
         */
        void start();
        
        /**
         * Realiza a troca de estado do jogo, modificando a tela atual.
         * 
         * @param gameState - Novo estado de jogo.
         */
        void changeGameState(GAME_STATE gameState);
        
        /**
         * Retorna a janela do programa.
         * 
         * @return Janela do programa.
         */
        sf::RenderWindow* getWindow();
        
        /**
         * Retorna o buffer de som de colisao.
         * 
         * @return Buffer de som de colisao.
         */
        sf::SoundBuffer* getHitSoundBuffer();
        
        /**
         * Retorna o mapa de textura do jogo.
         * 
         * @return Textura do jogo.
         */
        sf::Texture* getGameTexture();
        
        /**
         * Fecha a janela e encerra o programa.
         */
        void close();
        
        /**
         * Define o numero correspondente a skin da raquete da esquerda.
         * 
         * @param Numero correspondente a skin.
         */
        void setLeftPadModel(int padModel);
        
        /**
         * Retorna o numero correspondente a skin da raquete da esquerda.
         * 
         * @return Numero correspondente a skin.
         */
        int getLeftPadModel();
        
        /**
         * Define o numero correspondente a skin da raquete da direita.
         * 
         * @param Numero correspondente a skin.
         */
        void setRightPadModel(int padModel);
        
        /**
         * Retorna o numero correspondente a skin da raquete da direita.
         * 
         * @return Numero correspondente a skin.
         */
        int getRightPadModel();
        
        /**
         * Retorna a fonte usada na tela de opcoes.
         * 
         * @return Fonte usada na tela de opcoes.
         */
        sf::Font* getOptionFont();
        
        /**
         * Define a velocidade da bola.
         * 
         * @param Velocidade da bola.
         */
        void setBallSpeed(int ballSpeed);
        
        /**
         * Retorna a velocidade da bola.
         * 
         * @return Velocidade da bola.
         */
        int getBallSpeed();
};

#endif  // GAME_H

