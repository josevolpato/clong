/**
 * Paddle.hpp
 * 
 * Copyright (c) 2014 José Volpato
 * 
 * This file is part of Clong.
 *
 * Clong is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clong is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Clong.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef PADDLE_H
#define	PADDLE_H

#include <SFML/Graphics.hpp>

/**
 * Classe que representa uma raquete do jogo pong.
 */
class Paddle
{
    private:
        sf::Sprite* sprite;
        
    public:
        /**
         * Largura da raquete
         */
        static const int PADDLE_WIDTH = 20;
        
        /**
         * Altura da raquete
         */
        static const int PADDLE_HEIGHT = 110;        
        
        /**
         * Cria uma raquete do jogo pong.
         * 
         * @param x - Coordenada X da raquete.
         * @param y - Coordenada Y da raquete.
         * @param width - Largura da raquete.
         * @param height - Altura da raquete.
         * @param fillColor - Cor de preenchimento da raquete.
         */
        Paddle(int x, int y, sf::Sprite* sprite);
        
        /**
         * Finaliza o objeto raquete.
         */
        ~Paddle();
        
        /**
         * Retorna a coordenada X da raquete.
         * 
         * @return Coordenada X da raquete.
         */
        int getX();
        
        /**
         * Retorna a coordenada Y da raquete.
         * 
         * @return Coordenada Y da raquete.
         */
        int getY();
        
        /**
         * Define a coordenada X da raquete.
         * 
         * @param x - Coordenada X da raquete.
         */
        void setX(int x);
        
        /**
         * Define a coordenada Y da raquete.
         * 
         * @param y - Coordenada Y da raquete.
         */
        void setY(int y);
        
        /**
         * Retorna o sprite da raquete.
         * 
         * @return Sprite da raquete.
         */
        sf::Sprite* getSprite();
        
};

#endif	// PADDLE_H