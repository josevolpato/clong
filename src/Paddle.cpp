/**
 * Paddle.cpp
 * 
 * Copyright (c) 2014 José Volpato
 * 
 * This file is part of Clong.
 *
 * Clong is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clong is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Clong.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Paddle.hpp"

Paddle::Paddle(int x, int y, sf::Sprite* sprite)
{
    this->sprite = sprite;
    this->sprite->setPosition(x, y);
}

int Paddle::getX()
{
    return this->sprite->getPosition().x;
}

int Paddle::getY()
{
    return this->sprite->getPosition().y;
}

void Paddle::setX(int x)
{
    this->sprite->setPosition(x, getY());
}

void Paddle::setY(int y)
{
    this->sprite->setPosition(getX(), y);
}

sf::Sprite* Paddle::getSprite()
{
    return this->sprite;
}

Paddle::~Paddle()
{
    delete this->sprite;
}
