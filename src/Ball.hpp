/**
 * Ball.hpp
 * 
 * Copyright (c) 2014 José Volpato
 * 
 * This file is part of Clong.
 *
 * Clong is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Clong is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Clong.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef BALL_H
#define	BALL_H

#include <SFML/Graphics.hpp>

/**
 * Classe que representa uma bola do jogo Pong.
 */
class Ball : public sf::CircleShape
{
    private:
        int hDirection;
        int vDirection;
    
    public:
        /**
         * Cria um objeto bola.
         * 
         * @param x - Coordenada X da bola.
         * @param y - Coordenada Y da bola.
         * @param radius - Raio da bola.
         * @param fillColor - Cor de preenchimento da bola.
         */
        Ball(int x, int y, int radius, sf::Color fillColor);
        
        /**
         * Retorna a coordenada X da bola.
         * 
         * @return Coordenada X da bola.
         */
        int getX();
        
        /**
         * Retorna o coordenada Y da bola.
         * 
         * @return Coordenada Y da bola.
         */
        int getY();
        
        /**
         * Define a coordenada X da bola.
         * 
         * @param x Novo valor da coordenada X da bola.
         */
        void setX(int x);
        
        /**
         * Define a coordenada Y da bola.
         * 
         * @param y Novo valor da coordenada Y da bola.
         */
        void setY(int y);
        
        /**
         * Retorna a cor de preenchimento da bola.
         * 
         * @return Cor de preenchimento da bola.
         */
        sf::Color getColor();
        
        /**
         * Inverte a direcao horizontal da bola.
         */
        void invertHorizontalDirection();
        
        /**
         * Inverte a direcao vertical da bola.
         */
        void invertVerticalDirection();
        
        /**
         * Retorna a direcao horizontal da bola.
         * 
         * @return Direcao horizontal da bola.
         */
        int getHorizontalDirection();
        
        /**
         * Retorna a direcao vertical da bola.
         * 
         * @return Direcao vertical da bola.
         */
        int getVerticalDirection();        
};

#endif	// BALL_H